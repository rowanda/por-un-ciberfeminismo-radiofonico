# Por un ciberfeminismo radiofónico

## Queremos una radio ciberfeminista

Amamos la radio. Es un medio de comunicación tan potente como mágico. Los movimientos sociales la han usado para movilizar, para enseñar, para reivindicar derechos. Y allí donde identificaron limitaciones técnicas, como su unidireccionalidad, diseñaron mecanismos y dispositivos de participación para superarlas. Así, nuestras radios se convirtieron en centros sociales abiertos para hablar, escuchar, contar y debatir.

Las tecnologías digitales de comunicación nos ayudaron en ese proceso. Con su llegada podíamos aumentar la interactividad que le faltaba a la palabra dicha y escuchada, pero no contestada. Tecnologías que, por ser gratuitas y venir empaquetadas en una idea de acceso igualitario, adoptamos rápidamente. ¡Finalmente las radios comunitarias podíamos salir de la comunicación de nicho y cumplir el sueño de la masividad!

Pero, ¿qué tecnologías estamos usando? ¿Quiénes las desarrollan y en qué condiciones? ¿Cómo están hechas? ¿Cómo sabemos que hacen lo que dicen que hacen y no otras cosas? ¿Qué podemos hacer con ellas y, más importante, qué no podemos hacer? ¿Qué tipo de discursos censuran y cuáles amplifican? ¿Quiénes toman las decisiones en esas empresas? ¿Respetan los derechos humanos en toda su cadena de producción? ¿Cómo lograron estas empresas ser las más ricas del mundo? ¿Quién las controla y a quién rinden cuentas?

Históricamente desde el sector de la comunicación comunitaria nos hemos hecho estas preguntas respecto a los medios masivos. Pero por algún motivo dejamos de hacérselas a las tecnologías digitales. Nos convencimos de que eran simples herramientas neutrales que podíamos usar ‘bien o mal’ y no dispositivos ideológicos que encarnan los valores de quien las desarrolla.

Los principios de una radio (ciber/transhack)feminista entran en profunda contradicción con la cultura de Silicon Valley. Allí donde los hombres blancos heterosexuales toman las decisiones, nosotras proponemos horizontalidad e participación; allí donde la cultura del trabajo es explotadora, nosotras ponemos la vida en el centro y visibilizamos las tareas de cuidados; allí donde prima la cultura del hiperconsumismo, nosotras proponemos el cuidado del medioambiente; allí donde prima el fin de lucro como valor supremo, nosotras proponemos la defensa de los derechos humanos; allí donde se mercantilizan nuestros cuerpos y se nos cosifica, nosotras exigimos soberanía sobre ellos; allí donde hay individualismo, nosotras defendemos lo común.

¿Son las tecnologías neoliberales las que nos ayudarán en la lucha por una sociedad feminista?. Por suerte tenemos alternativas. Podemos usar software desarrollado por la comunidad y distribuido de manera libre, alargar el tiempo de vida de nuestras máquinas el mayor tiempo posible para reducir la basura electrónica y el extractivismo, minimizar el uso de datos para garantizar nuestra libertad y reducir el consumo energético, construir nuestras propias infraestructuras feministas comunitarias y autónomas para que nuestra memoria como movimiento no esté bajo el dominio de quienes producen y reproducen este mundo desigual.

Este fanzine/web es un pequeño aporte a la construcción de una radio feminista soberana tecnológicamente, ¡una radio ciber/transhackfeminista!. Estamos cuestionando la ausencia de nuestras voces en la radio, la reproducción de estereotipos machistas, las agendas cisheteropatriarcales o la concentración de la propiedad mediática. Pero nos quedaremos cortas si las tecnologías digitales de comunicación se escapan de nuestro ámbito de crítica y acción política.

## Claves para la producción radiofónica feminista

1. **Pensemos en nuestra audiencia.** Si hacemos radio es para que nos escuchen.
2. **¡A prepararnos!** Tenemos que trabajar, y mucho, incluso cuando se trate de piezas cortas.
3. **Fomentemos la imaginación**. Seamos creativas y usemos los sonidos para crear escenarios.
4. **Asumamos los aspectos técnicos**. Este también es un ámbito de empoderamiento. Ocupemos y echemos mano a las máquinas, los cables, las fichas, el software, etc.
5. **Diversidad de voces y voces diversas**. Queremos dejar de escuchar las mismas historias. Queremos contar nuestras alegrías, preocupaciónes, penas y sueños, queremos que corra la voz y se escuchen los mundos que queremos construir.
6. **Cuidemos la calidad**. Los contenidos interesantes también necesitan sonar bien.
7. **No aburramos ni nos aburramos**. Si no nos divertimos haciendo radio menos lo harán quienes nos escuchen.
8. **Trabajemos en red**. Somos muchas haciendo trabajando con sonidos: compartamos y aprendamos entre todas.
9. **Salgamos de nuestras casas y estudios**. Descubramos todo lo que la realidad tiene para contarnos.
10. **Pongamos la vida en el centro**. Si no llegamos, no llegamos. El autocuidado como estrategia de producción.
11. **Conozcamos todos los recursos**. Aprovechemos todo el espectro del lenguaje radiofónico: sonidos, efectos, voces, ruidos, silencios, músicas de todas partes.
12. **Usemos lenguaje sencillo e inclusivo**. Hablemos para ser escuchadas, no leídas, y recordemos que el lenguaje instituye sentido.
13. ∞

## Prototipado

- Título y sinopsis. Esta es la primera idea que tenemos para nuestro podcast. ¿Cómo lo describirías en una oración? ¿Cómo se va a llamar?
- Tema, recorte y enfoque. Sobre qué vas a hablar, en qué aspecto vas a hacer énfasis y desde qué perspectiva.
- Audiencia. ¡Importantísmo! ¿A quiénes vamos a hablarle? ¿Quién nos gustaría que nos escuche? ¿Qué cosas le gustan a ese grupo de personas? ¿Cuáles son sus referentes?
- Identidad sonora. ¿Cómo nos gustaría sonar? ¿Qué ambientes queremos construir? ¿Qué emociones queremos transmitir? Todo eso lo generamos con nuestra identidad sonora. Tenemos que lograr que quien nos escuche sepa inmediatamente que somos nosotras. Los elementos sonoros serán nuestro sello de identidad.
- Género. Periodístico, ficcional, documental, experimental, musical, deportivo, entretenimiento, etc.
- Formato. Crónica, magazine, reportajes, _storytelling_, entrevistas, cuñas, radiodrama, miniradio, tertulia, emotifones, sonomensajes, etc.
- Estructura. ¿Cómo vamos a contar nuestra historia? Duración, capítulos, temporadas, orden en que se presenta el tema, escenas, libreto o guión, quién guía el relato, etc.
- Alojamiento y distribución. ¿A través de qué canales podrán escucharnos? ¿Lo haremos en vivo? ¿Lo alojaremos en nuestra página o en otras plataformas? ¿Qué agregadores utilizaremos?
- Identidad gráfica. Diseñemos una imagen que identifique a nuestro podcast en los distintos canales de distribución.
- Licencia. Una licencia nos permitirá dar de antemano permisos de uso sobre nuestras piezas radiofónicas.

## Recursos libres

## Software libre para la producción

### VLC
Este software no sólo reproduce cualquier cosa sino que también nos ayuda a convertir formatos, cortar audios o aplicar algunos efectos.

### Conversor
Este conversor nos permitirá pasar formatos para poder trabajar como más nos guste.

### Audacity
Este pequeño programa de edición de audio multipista es mucho más potente de lo que parece a primera vista. Las animamos a descubrir todas sus funcionalidades

### Ardour
Con Ardour ya estamos hablando de palabras mayores. Es el editor de software libre más completo por eso agarrarle la mano requiere un poco más

## ¡Que nos escuchen!

### _Streaming_
Podemos usar el software multiplataforma BUTT con un punto de montaje gratuito del servidor de IceCast GISS.

### Archivo

- Archive
- Funkwhale
- Opentube
- Nextcloud (Disroot)

### RSS

Este sistema de distribución de contenidos permite que la gente se suscriba a nuestro podcast. Algunas plataformas generan el _feed_ de RSS automáticamente. También lo podemos hacer nosotras. Averiguá cómo en [Radios Libres](https://www.radioslibres.net/crea-tu-propio-rss).

### Aplicaciones

- VLC - videolan.org/vlc
- Vocal - vocalproject.net
- Clementine - clementine-player.org

## Licencias

El conocimiento es un bien común de la humanidad. Sólo compartiéndolo podemos alimentarlo y expandirlo. Por eso, frente a la mercantilización de la cultura y el conocimiento, las licencias libres nos permiten distribuir libremente los contenidos que produzcamos. Existen disntinas licencias libres que pueden adecuarse al tipo de permisos que queramos dar: de escuchar, de distribuir, de modificar, de vender, de hacer obras nuevas, etc.

Para licenciar nuestros podcast simplemente debemos elegir el tipo de licencia que queramos y explicitarlo. Puede ser en los metadatos del podcast, en la página web dónde lo subamos o en nuestro feed de RSS. Algunas licencias a explorar son: Creative Commons, [Licencia Feminista de Producción de Pares (f2f)](https://labekka.red/licencia-f2f), GPL, etc.

## Licencias

Este fanzine/web tiene una Licencia Feminista de Producción de Pares (f2f). Esto significa que, reconociendo su autoría, se permite compartirla (copiarla, distribuirla o comunicarla públicamente) y hacer obras derivadas y explotarlas comercialmente solamente si eres parte de una cooperativa, organización o colectiva sin fines de lucro, u organización de trabajadoras autogestionadas, que defiendan y se organicen bajo principios feministas. Además, debes compartir cualquier obra derivada esta obra con la misma licencia.
