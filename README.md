# Por un ciberfeminismo radiofónico

Este es el repositorio del fanzine dedicado a la intersección entre radio comunitaria, feminismos y tecnologías digitales, desarrollado desde [Radios Libres](https://radioslibres.net) y el [Centro de Producciones Radiofónicas](https://cpr.org.ar). 

Las invitamos a participar sugiriendo correcciones, ediciones y recursos. También se puede descargar el contenido y utilizarlo como prefieran. 

![Por un ciberfeminismo radiofónico](/media/portada2.png)

## Licencia Feminista de Pares (f2f)

Esta obra se libera bajo la licencia Producción Feminista de Pares. Esto significa que, reconociendo su autoría, se permite compartirla (copiarla, distribuirla o comunicarla públicamente) y hacer obras derivadas y explotarlas comercialmente solamente si eres parte de una cooperativa, organización o colectiva sin fines de lucro, u organización de trabajadoras autogestionadas, que defiendan y se organicen bajo principios feministas. Además, debes compartir cualquier obra derivada esta obra con la misma licencia.

Para más información, visita https://labekka.red/licencia-f2f

## Contacto

Si tienes dudas, escríbenos a ines@cpr.org.ar
